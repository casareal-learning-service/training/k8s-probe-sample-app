K8s Probe Sample App
====================

KubernetesのProbeを体験するためのサンプルアプリです。

# 起動
- 起動直後に何秒かスリープします。このスリープ時間は環境変数 `STARTUP_SECONDS` に正の整数で指定します。
    - 3秒スリープさせる例 `export STARTUP_SECONDS=3`
    - 環境変数 `STARTUP_SECONDS` が無かった場合、スリープ時間は5秒になります。
- 起動後は、全Probeに対して正常なレスポンス（200 OK）を返します（この挙動は `/change` で変更可能）。
- ポート番号は `8080` です。

# エンドポイント一覧
- `/started`
    - Startup Probeへのレスポンスを返します。
    - スリープ中は500 Internal Server Errorを返し、それ以降はずっと200 OKを返します。
    - curlコマンドの例 `curl -i localhost:8080/started`
- `/live`
    - Liveness Probeへのレスポンスを返します。
    - 変数 `live` が `true` であれば200 OK、そうでなければ500 Internal Server Errorを返します。
    - 変数 `live` は、起動完了後は `true` です。値を変更する場合は `/change` を利用してください。
    - curlコマンドの例 `curl -i localhost:8080/live`
- `/ready`
    - Readiness Probeへのレスポンスを返します。
    - 変数 `ready` が `true` であれば200 OK、そうでなければ500 Internal Server Errorを返します。
    - 変数 `ready` は、起動完了後は `true` です。値を変更する場合は `/change` を利用してください。
    - curlコマンドの例 `curl -i localhost:8080/live`
- `/change`
    - 変数 `live` や `ready` の値を変更します。
    - 値はクエリパラメータで指定します。
        - 例: `/change?live=false` `/change?ready=false` 
    - curlコマンドの例 `curl -i "localhost:8080/change?live=false"` `curl -i "localhost:8080/change?ready=false"`
- `/`
    - 常に200 OKと `Hello!` という文字列を返します。
    - curlコマンドの例 `curl -i localhost:8080/`

# Dockerイメージ
registry.gitlab.com/casareal-learning-service/training/k8s-probe-sample-app:latest
