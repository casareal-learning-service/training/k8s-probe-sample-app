package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"
)

var (
	started = false
	live = false
	ready = false
)

func main() {
	// スリープ後、各変数をtrueにする（非同期）
	go sleepAndStart()

	// Startup Probe
	http.HandleFunc("/started", startupProbe)
	// Liveness Probe
	http.HandleFunc("/live", livenessProbe)
	// Readiness Probe
	http.HandleFunc("/ready", readinessProbe)
	// LivenessやReadinessの状態を変更するエンドポイント
	http.HandleFunc("/change", change)
	// "Hello!"を返すだけのエンドポイント
	http.HandleFunc("/", hello)

	// ポート番号8080で起動
	log.Fatal(http.ListenAndServe(getPort(), nil))
}

func getPort() string {
	port := os.Getenv("SERVER_PORT")
	if port != "" {
		return port
	} else {
		return ":8080"
	}
}

func sleepAndStart() {
	// 環境変数からスリープ時間を取得
	secondsStr := os.Getenv("STARTUP_SECONDS")
	if secondsStr == "" {
		// 環境変数が設定されていない場合はデフォルトで5秒スリープ
		log.Printf("Sleep 5 seconds...\n")
		time.Sleep(time.Second * 5)
	} else if seconds, err := strconv.Atoi(secondsStr); err != nil || seconds <= 0 {
		// 環境変数が正の整数でない場合はパニックして終了
		log.Panicf("STARTUP_SECONDS must be positive integer: %v\n", err)
	} else {
		// 環境変数で指定されただけスリープ
		log.Printf("Sleep %d seconds...\n", seconds)
		time.Sleep(time.Second * time.Duration(seconds))
	}

	// 各Probeが正常なレスポンスを返すようにする
	started = true
	live = true
	ready = true
	log.Println("Server is ready")
}

func hello(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(200)
	message := "Hello!"
	fmt.Fprintf(w, message)
	log.Println(message)
}

func probe(probeName string, status bool, w http.ResponseWriter) {
	var statusCode int
	var message string
	if status {
		statusCode = 200
		message = "success"
	} else {
		statusCode = 500
		message = "failed"
	}
	w.WriteHeader(statusCode)
	fmt.Fprintf(w, message)
	log.Printf("%s %s", probeName, message)
}

func startupProbe(w http.ResponseWriter, r *http.Request) {
	probe("Startup Probe", started, w)
}

func livenessProbe(w http.ResponseWriter, r *http.Request) {
	probe("Liveness Probe", live, w)
}

func readinessProbe(w http.ResponseWriter, r *http.Request) {
	probe("Readiness Probe", ready, w)
}

func change(w http.ResponseWriter, r *http.Request) {
	if queryParams, ok := r.URL.Query()["live"]; ok {
		if value, err := strconv.ParseBool(queryParams[0]); err != nil {
			w.WriteHeader(400)
			message := "live must be bool"
			fmt.Fprintf(w, message)
			log.Println(message)
		} else {
			live = value
			w.WriteHeader(200)
			message := fmt.Sprintf("live was changed to %v", live)
			fmt.Fprintf(w, "live was changed to %v", live)
			log.Println(message)
		}
		return
	}
	if queryParams, ok := r.URL.Query()["ready"]; ok {
		if value, err := strconv.ParseBool(queryParams[0]); err != nil {
			w.WriteHeader(400)
			message := "ready must be bool"
			fmt.Fprintf(w, message)
			log.Println(message)
		} else {
			ready = value
			w.WriteHeader(200)
			message := fmt.Sprintf("ready was changed to %v", ready)
			fmt.Fprintf(w, message)
			log.Println(message)
		}
		return
	}
	w.WriteHeader(400)
	message := "query param 'live' or 'ready' must be specified"
	fmt.Fprintf(w, message)
	log.Println(message)
}
