# ビルド用コンテナのベースイメージ（Goコンパイラが含まれている）
FROM golang:1.15.4-alpine3.12 AS build_base

# コンテナ内の/tmp/sample-appをカレントディレクトリとする
WORKDIR /tmp/sample-app

# 必要なファイルをコンテナ内にコピー
COPY go.mod .
COPY server.go .

# アプリケーションのビルド
RUN go build

#########################################################
# アプリケーション用コンテナのベースイメージ
FROM alpine:3.12

# ビルド用コンテナから、ビルド済みのアプリケーションバイナリをコピー
COPY --from=build_base /tmp/sample-app/k8s-probe-sample-app /app/k8s-probe-sample-app

# ポート番号1323を公開する（実行時に-pで指定が必要）
EXPOSE 8080

# コンテナが起動したら、ビルド済みのアプリケーションバイナリを起動
CMD ["/app/k8s-probe-sample-app"]
